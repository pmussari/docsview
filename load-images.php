<?php

    if( php_sapi_name() !== 'cli' ) {
        die("Meant to be run from command line");
    }

    if(!$argv[1]){
        die("Incluir directorio de importacion");   
    }
    if(!$argv[2]){
        die("Incluir ID de empresa");
    }

    $METAINDICES = array(
        1 => "numerolegajo",
        2 => "numerocaja",
        4 => "numerosobre"
    );

    $dirimportador = $argv[1];
    
    function find_wordpress_base_path() {
        $dir = dirname(__FILE__);
        do {
            //it is possible to check for other files here
            if( file_exists($dir."/wp-config.php") ) {
                return $dir;
            }
        } while( $dir = realpath("$dir/..") );
        return null;
    }

    define( 'BASE_PATH', find_wordpress_base_path()."/" );
    define('WP_USE_THEMES', false);
    global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
    require(BASE_PATH . 'wp-load.php');
    require_once( BASE_PATH . 'wp-admin/includes/image.php' );    
    $wp_upload_dir = wp_upload_dir();
    
    $directory = $wp_upload_dir['basedir'] .'/'. $dirimportador . '/';
    $scanned_directory = array_diff(scandir($directory), array('..', '.'));
    foreach ($scanned_directory as $file) {
        $filename = $directory . $file;
        $filetype = wp_check_filetype( basename( $filename ), null );
        preg_match('/^([0-9]*)_(CAJA_([\d]*))_(SOBRE_([\d]*))/', $file,$matches);
        
        
        //$matches = sscanf($file, '%d_CAJA_%d_SOBRE_%d.jpg');
        $attachment = array(
            'guid'           => $wp_upload_dir['baseurl'] . '/' . $dirimportador . '/' . $file, 
            'post_mime_type' => $filetype['type'],
            'post_title'     => preg_replace( '/\.[^.]+$/', '', $file ),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        // Insert the attachment.
        $attach_id = wp_insert_attachment( $attachment, $filename );
        if ( !is_wp_error($attach_id) ) {
            $term = wp_set_post_terms( $attach_id , $argv[2] , "empresa", false );
            foreach ($METAINDICES as $key => $value) {
                $meta_key = $value;
                $meta_value = $matches[$key];
                add_post_meta( $attach_id , $meta_key, $meta_value, true );
            }
        }

    }



?>