<?php
/**
 *
 * @package DocsFile
 * @subpackage Docs_View
 * @since Hospital Aleman
 */

$COMPANYPARAMS = array();

function company_init() {
	// create a new taxonomy
	register_taxonomy(
		'empresa',
		'post',
		array(
			'label' => __( 'Empresa' ),
			'rewrite' => array( 'slug' => 'empresa' ),
			'capabilities' => array(
				
			)
		)
	);
	global $COMPANYPARAMS;
	$COMPANYPARAMS["Utedyc"] = array(
		"show_date" => false,
		"metas" => array("numerolegajo","numerocaja","numerosobre"),
		"metas_title" => array("Legajo","Caja","Sobre")
	);
	$COMPANYPARAMS["default"] = array(
		"show_date" => true,
		"metas" => array(),
		"metas_title" => array()
	);
}

add_action( 'init', 'company_init' );

/*Devolvemos la HOME como la url que solicito*/
function home_for_host(){
	return "http://" . $_SERVER['SERVER_NAME'];
}
add_filter("pre_option_home","home_for_host");

function themeuri_for_host(){
	return "http://" . $_SERVER['SERVER_NAME'] . "/wp-content/themes";
}
add_filter("theme_root_uri","themeuri_for_host");

function siteurl_for_host($url,$path){
	return "http://" . $_SERVER['SERVER_NAME'] . "/" . $path;
}
add_filter('site_url',  'siteurl_for_host', 10, 3);

/**
 * Enqueue scripts and styles for the front end.
 */
function docsview_scripts() {
	wp_enqueue_style( 'docsview', get_stylesheet_uri() );
	wp_enqueue_style( 'docsview-bootstrap', get_template_directory_uri() . "/css/bootstrap.min.css" );
	wp_enqueue_style( 'docsview-fontawesome', get_template_directory_uri() . "/css/font-awesome.min.css" );
	wp_enqueue_style( 'docsview-angular-datatables', get_template_directory_uri() . "/css/angular-datatables.min.css" );
	wp_enqueue_style( 'docsview-bootstrap-datatables', get_template_directory_uri() . "/css/datatables.bootstrap.min.css" );
	
	wp_enqueue_script( 'docsview-jquery', get_template_directory_uri() . '/js/jquery.min.js');
	wp_enqueue_script( 'docsview-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js');
	wp_enqueue_script( 'docsview-datatables', get_template_directory_uri() . '/js/jquery.dataTables.min.js');

	wp_register_script( 'docsview-angular', get_template_directory_uri() . '/js/angular.min.js', null, false);
	wp_register_script( 'docsview-angular-router', get_template_directory_uri() . '/js/angular-ui-router.min.js', array('docsview-angular'), null, false);
	wp_register_script( 'docsview-angular-datatables', get_template_directory_uri() . '/js/angular-datatables.min.js', array('docsview-angular'), null, false);
	wp_register_script( 'docsview-angular-datatables-bootstrap', get_template_directory_uri() . '/js/angular-datatables.bootstrap.min.js', array('docsview-angular'), null, false);
	
	wp_register_script( 'docsview-angular-bootstrap-ui', get_template_directory_uri() . '/js/ui-bootstrap.min.js', array('docsview-angular'), null, false);
	wp_register_script( 'docsview-angular-bootstrap-ui-tpls', get_template_directory_uri() . '/js/ui-bootstrap-tpls.min.js', array('docsview-angular-bootstrap-ui'), null, false);

	wp_register_script( 'docsview-app', get_bloginfo('template_directory').'/js/app.js', array('docsview-angular'), null, false);

	wp_enqueue_script('docsview-angular');
	wp_enqueue_script('docsview-angular-router');
	wp_enqueue_script('docsview-angular-datatables');
	wp_enqueue_script('docsview-angular-datatables-bootstrap');
	wp_enqueue_script('docsview-angular-bootstrap-ui');
	wp_enqueue_script('docsview-angular-bootstrap-ui-tpls');
	wp_enqueue_script('docsview-app');

	$slug = get_user_company_slug();
	$data = array( 
		'url' => get_bloginfo('template_directory').'/', 
		'site' => $_SERVER['HOST'],
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'is_logged_in' => is_user_logged_in(),
        'empresa' => is_user_logged_in() ? get_user_company() : 'DocsView'
   	);
   	
   	if($slug && file_exists( __DIR__ . "/img/" . $slug . ".jpg" )){
   		$data["logo"] = get_bloginfo('template_directory'). "/img/" . $slug . ".jpg";
   	}
   	if($slug && file_exists( __DIR__ . "/css/" . $slug . ".css" )){
   		wp_enqueue_style( 'docsview-empresa', get_template_directory_uri() . "/css/" . $slug . ".css" );
   	}
	$data["params"] = get_params_company();
	wp_localize_script( 'docsview-app', 'AppConfig', $data );

}

add_action( 'wp_enqueue_scripts', 'docsview_scripts' );

function get_params_company(){
	global $COMPANYPARAMS;
	$company = get_user_company();
	if(array_key_exists($company, $COMPANYPARAMS)){
		return $COMPANYPARAMS[$company];
	}else{
		return $COMPANYPARAMS['default'];
	}
}

function get_user_company(){
	//Return taxonomy term company
	$user = get_current_user_id();
	if($user == 5){
		return "Utedyc";
	}
	return "Hospital Aleman";
}

function get_user_company_term(){
	$empresa = get_user_company();
	return get_company_term($empresa);
}

function get_company_term($empresa){
    $term = term_exists($empresa, 'empresa');
    if($term){
        return array( intval($term['term_id']) );
    }else{
        return false;
    }
}

function get_user_company_slug(){
	$empresa = get_user_company();
	$term = term_exists($empresa, 'empresa');
	if($term){
		$term_data = get_term_by('id', $term['term_id'] , 'empresa');
		return $term_data->slug;
	}else{
		return false;
	}
}

function docsview_upload(){
	$return = array();
	$post_id = media_handle_upload('file',0,array('status'=>'publish'));
	if(is_wp_error($post_id)){
		status_header(503);
		wp_send_json($return);	
	}
	$return['post_id'] = $post_id;

	//Le marcamos la empresa a la que corresponda el usuario
	$term = wp_set_post_terms( $post_id , get_user_company_term(), "empresa", false );
	$return['term'] = $term;
	foreach ($_POST as $key => $value) {
		if( preg_match("/^meta_/", $key) ){
			$meta_key = str_replace("meta_", "", $key);
			$meta_value = $value;
			add_post_meta( $post_id , $meta_key, $meta_value, true );
		}
	}
	wp_send_json($return);
}

add_action( 'wp_ajax_docsview_upload', 'docsview_upload' );

function get_args_request(){
	$args = array(
		'post_type' => 'attachment',
		'post_status' => 'any',
		'posts_per_page' => $_REQUEST["length"],
		'offset' => $_REQUEST["start"],
		'tax_query' => array(
			array(
				'taxonomy' => 'empresa',
				'field'    => 'term_id',
				'terms'    => get_user_company_term()
			),
		),
	);

	if( array_key_exists('fechad', $_GET) || array_key_exists('fechah', $_GET) ){
		$date_query = array();
		$date_query[] = array();
		if(array_key_exists('fechad', $_GET)){
			$date_query[0]["after"] = $_GET["fechad"];	
		}
		if(array_key_exists('fechah', $_GET)){
			$date_query[0]["before"] = $_GET["fechah"];
		}
		$date_query[0]["inclusive"] = true;
 	}
 	
 	if(isset($date_query)){
 		$args['date_query'] = $date_query;
 	}
	$params = get_params_company();
 	$order = $_GET['columns'][$_GET['order'][0]['column']]['data'];
 	if( $order == 'fecha'){
 		$args['orderby'] = 'date';
		$args['order'] = $_GET['order'][0]['dir'];
 	}else if( $order == 'id' ){
 		$args['orderby'] = 'ID';
		$args['order'] = $_GET['order'][0]['dir'];
 	}else{
		foreach ($params["metas"] as $meta_key) {
			if( $order == $meta_key ){
		 		$args['orderby'] = 'meta_value_num';
				$args['meta_key']  = $meta_key;
				$args['order'] = $_GET['order'][0]['dir'];
		 	}		
		}
 	}

 	if( strlen($_GET['search']['value']) > 0 ){
 		$args['meta_query'] = array(
 			'relation' => 'OR'
		);
		foreach ($params["metas"] as $meta_key) {
			$args['meta_query'][] =  array(
				'key'     => $meta_key,
				'value'   => $_GET['search']['value'],
				'compare' => 'LIKE',
			);
		}
	} 
	
 	return $args;
}

function complete_metas(&$object){
	$params = get_params_company();
	foreach ($params["metas"] as $meta_key) {
		$object[$meta_key] = get_post_meta( get_the_ID() , $meta_key , true);
	}
}

function docsview_export(){

	$filename = "listado-".substr(md5(time()),6).".csv";
	header("Content-Disposition: attachment; filename=\"$filename\"");
  	header("Content-Type: application/vnd.ms-excel");
  	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	$args = get_args_request();	
	$query = new WP_Query( $args );
	$params = get_params_company();
	echo implode(array_values($params["metas_title"]) ," , ") . ", Fecha de Carga , Link Documento \n";
	while ( $query->have_posts() ) {
		$query->the_post();
		$post = get_post();
		$object = array();
		//Solo los meta del tipo de documento
		complete_metas($object);
		$object["fecha"] = $post->post_date;
		$object["url"] = wp_get_attachment_url($post->ID);
		echo implode(array_values($object)," , "). " \n";
		unset($object);
	}
	wp_die();
}

add_action( 'wp_ajax_docsview_export', 'docsview_export' );

function docsview_list(){

	$args = get_args_request();
	
	$return = array();
	$query = new WP_Query( $args );
	while ( $query->have_posts() ) {
		$query->the_post();
		$post = get_post();
		$object = array("id"=> $post->ID );
		$object["fecha"] = $post->post_date;
		$object["url"] = wp_get_attachment_url($post->ID);
		//Solo los meta del tipo de documento
		complete_metas($object);
		array_push($return, $object);
	}
	$pag = array(
	    "draw" => $_REQUEST["draw"],
	    "recordsTotal" => $query->found_posts,
	    "recordsFiltered" => $query->found_posts,
	    "data" => $return
	);
	wp_send_json($pag);
}

add_action( 'wp_ajax_docsview_list', 'docsview_list' );

function docsview_delete(){
	$return = wp_delete_post($_POST['id']);
	wp_send_json($return);
}

add_action( 'wp_ajax_docsview_delete', 'docsview_delete' );

function docsview_login(){

    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
    	status_header(403);
        wp_send_json(array('is_logged_in'=>false, 'message'=>__('Wrong username or password.')));
    } else {
   	    wp_set_current_user( $user_signon->data->ID, $user_signon->data->user_login );
   	    $slug = get_user_company_slug();
   	    $data = array(
   	    	'is_logged_in'=>true, 
   	    	'empresa' => get_user_company(), 
   	    	'slug' => $slug
   	    );
    	if($slug && file_exists( __DIR__ . "/img/" . $slug . ".jpg" )){
	   		$data["logo"] = get_bloginfo('template_directory'). "/img/" . $slug . ".jpg";
	   	}
	   	if($slug && file_exists( __DIR__ . "/css/" . $slug . ".css" )){
	   		$data["style"] = get_template_directory_uri() . "/css/" . $slug . ".css";
	   	}
	   	$data["params"] = get_params_company();
        wp_send_json($data);
    }
}

add_action( 'wp_ajax_nopriv_docsview_login', 'docsview_login' );

function docsview_logout(){

	wp_logout();

}
add_action( 'wp_ajax_docsview_logout', 'docsview_logout' );

function s3_reads_wp_get_attachment_image_attributes($array)
{
  $array['src'] = s3_reads_replace_image_url($array['src']);
  return $array;
}
add_filter( 'wp_get_attachment_image_attributes', 's3_reads_wp_get_attachment_image_attributes' );

function s3_reads_wp_get_attachment_url($url)
{
  $url = s3_reads_replace_image_url($url);
  return $url;
}
add_filter( 'wp_get_attachment_url', 's3_reads_wp_get_attachment_url' );

function s3_reads_replace_image_url($url)
{
	if(function_exists('w3_instance')){
		$config = w3_instance('W3_Config');	
		if($config){
			$bucket = $config->get_string('cdn.s3.bucket');
			$pattern = '/^http:\/\/(.)*\/wp-content/';
			return preg_replace($pattern, "http://{$bucket}.s3.amazonaws.com/wp-content", $url);
		}	
	}
	return $url;
}
