<?php
/**
 *
 * @package DocsFile
 * @subpackage Docs_View
 * @since Hospital Aleman
 */

?><!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php wp_head(); ?>
</head>

<body>
	<div class="loading">Loading&#8230;</div>
	<div ng-app="docsViewApp" ng-class="contentClass" ng-controller="AppController">
		<div ui-view="main"></div>
	</div>

