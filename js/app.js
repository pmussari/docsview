(function(docsViewApp){

	docsViewApp.config(function($stateProvider,$locationProvider,$urlRouterProvider){
		$stateProvider.state('login',{
			url:'/login',
			views:{
				'main':{
			    	templateUrl: AppConfig.url + 'templates/login.tpl.html',
			    	controller: 'LoginController'
				}
			},data: {
				contentClass: 'login'
			}
		}).state('home',{
			views:{
				'main':{
			    	templateUrl: AppConfig.url + 'templates/home.tpl.html',
			    	controller: 'HomeController'
				}
			},data: {
				contentClass: 'home'
			}
		}).state('home.listado',{
			url:'/listado',
			views:{
				'header':{
			    	templateUrl: AppConfig.url + 'templates/header.tpl.html',
			    	controller: 'HeaderController'
				},
				'body':{
			    	templateUrl: AppConfig.url + 'templates/listado.tpl.html',
			    	controller: 'ListadoController'
				}
			},
			parent:'home',
			resolve: {
				legajos: function(LegajoService){
					return LegajoService.listado();
				}
			}
		}).state('home.busqueda',{
			url:'/listado/:fechad/:fechah',
			views:{
				'header':{
			    	templateUrl: AppConfig.url + 'templates/header.tpl.html',
			    	controller: 'HeaderController'
				},
				'body':{
			    	templateUrl: AppConfig.url + 'templates/listado.tpl.html',
			    	controller: 'ListadoController'
				}
			},
			parent:'home',
			resolve: {
				legajos: function(LegajoService,$stateParams){
					return LegajoService.listado($stateParams.fechad,$stateParams.fechah);
				}
			}
		}).state('home.nuevo',{
			url:'/nuevo',
			views:{
				'header':{
			    	templateUrl: AppConfig.url + 'templates/header.tpl.html',
			    	controller: 'HeaderController'
				},
				'body':{
			    	templateUrl: AppConfig.url + 'templates/nuevo.tpl.html',
			    	controller: 'NuevoController'
				}
			},
			parent:'home',
			data: {
				contentClass: 'nuevo'
			}
		});		
		//$locationProvider.html5mode(true);
		$urlRouterProvider.otherwise('listado');
	});

	docsViewApp.run(function($state){
		
	});

	docsViewApp.controller('AppController',function($rootScope,$state){
		$rootScope.$on("$stateChangeSuccess",function(event, toState, toParams, fromState, fromParams){
			angular.element(".loading").hide();
			if (angular.isDefined(toState.data.contentClass)) {
		        $rootScope.contentClass = toState.data.contentClass;
		        return;
	    	}
		});
		$rootScope.$on("$stateChangeStart",function(event, toState, toParams, fromState, fromParams){
			angular.element(".loading").show();
			console.log(AppConfig.is_logged_in);
			if(toState.parent === 'home' && AppConfig.is_logged_in !== "1" &&  AppConfig.is_logged_in !== true){
				event.preventDefault();
				$state.go('login');
				angular.element(".loading").hide();
			}
		});	
	});

	docsViewApp.controller('LoginController',function($scope,$state,UserService){
		$scope.login = function(){
			$scope.loading = true;
			UserService.login($scope.user).then(function(user){
				AppConfig.is_logged_in = user.is_logged_in;
				AppConfig.empresa = user.empresa;
				AppConfig.logo = user.logo;
				AppConfig.params = user.params;
				if( user.style ){
					angular.element('head').append('<link id="docsview-empresa-css" rel="stylesheet" type="text/css" href="'+user.style+'">');
				}
				$scope.loading = false;		
				$state.go('home');
			},function(error){
				$scope.loading = false;
				$scope.errormsj = error.message;
			});
		};
		$scope.loading = false;
	});

	docsViewApp.controller('HomeController',function($state){
		if($state.current.name == 'home'){
			$state.go('home.listado');
		}
	});

	docsViewApp.controller('HeaderController',function($scope,$state,UserService){
		$scope.logout = function(){
			UserService.logout().then(function(){
				AppConfig.is_logged_in = false;
				AppConfig.empresa = '';
				AppConfig.logo = '';
				angular.element("#docsview-empresa-css").remove()
				$state.reload();
			});
		};
		$scope.logo = AppConfig.logo;
		$scope.empresa = AppConfig.empresa;
	});

	docsViewApp.controller('ListadoController',function($scope,$state,$rootScope,$window,$uibModal,LegajoService,DTOptionsBuilder,DTColumnBuilder,DTColumnDefBuilder,$filter,$stateParams,$compile){
		var url_list = AppConfig.ajaxurl + '?action=docsview_list';
		$scope.bsTable = { };
		$scope.bsTable.dtInstance = { };
		$scope.showDate = AppConfig.params.show_date;
    	if($stateParams.fechad){
    		$scope.fechad = new Date($stateParams.fechad + " 12:00:00");
    		url_list += '&fechad=' + $stateParams.fechad;
    	}
    	if($stateParams.fechah){
    		$scope.fechah = new Date($stateParams.fechah + " 12:00:00");	
    		url_list += '&fechah=' + $stateParams.fechah;
    	}
    	
	 	$scope.bsTable.dtOptions = DTOptionsBuilder.newOptions()
	        .withOption('ajax', {
	         	url: url_list,
	         	type: 'GET'})
	     	.withDataProp('data')
	     	.withOption('responsive', true)
	        .withOption('processing', true)
	        .withOption('serverSide', true)
			.withOption('createdRow', function(row, data, dataIndex) {
            	$compile(angular.element(row).contents())($scope);
        	})
	        .withPaginationType('full')
	        .withLanguage({
    		"sEmptyTable": "No hay datos disponibles para la busqueda",
    		"sInfo":           "Mostrando _START_ hasta _END_ de _TOTAL_ legajos",
		    "sInfoEmpty":      "",
		    "sInfoFiltered":   "(filtered from _MAX_ total entries)",
		    "sInfoPostFix":    "",
		    "sInfoThousands":  ",",
		    "sLengthMenu":     "Mostrar _MENU_ ",
		    "sLoadingRecords": "Cargando...",
		    "sProcessing":     "Procesando...",
		    "sSearch":         "Busqueda:",
		    "sZeroRecords":    "No hay datos disponibles para la busqueda",
		    "oPaginate": {
		        "sFirst":    "Inicio",
		        "sLast":     "Ultima",
		        "sNext":     "Siguiente",
		        "sPrevious": "Anterior"
		    }
    	}).withBootstrap();

	    $scope.bsTable.dtColumns = [];
	    $scope.bsTable.dtColumns.push(DTColumnBuilder.newColumn('id').withTitle('ID').notVisible());
		angular.forEach(AppConfig.params.metas,function(meta,index){
			$scope.bsTable.dtColumns.push(DTColumnBuilder.newColumn(meta).withTitle(AppConfig.params.metas_title[index]));
		});
	    if(AppConfig.params.show_date){
	    	$scope.bsTable.dtColumns.push(DTColumnBuilder.newColumn('fecha').withTitle('Fecha'));	
	    }else{
	    	$scope.bsTable.dtColumns.push(DTColumnBuilder.newColumn('fecha').withTitle('Fecha').notVisible());
	    }
	    $scope.bsTable.dtColumns.push(
           	DTColumnBuilder.newColumn(null).withTitle("").notSortable().renderWith(function(data, type, full, meta) {
                return '<button class="btn btn-primary" ng-click="ver('+meta.row+')">'+
								'<i class="fa fa-paperclip" aria-hidden="true"></i>'+
							'</button> '+
							'<button class="btn btn-danger" ng-click="eliminar('+meta.row+')">'+
								'<i class="fa fa-trash-o" aria-hidden="true"></i>'+
							'</button>';
	    	})
		); 

		$scope.ctrlFechad = {
			opened : false,
			open:function(){
				this.opened = true;
			}
		};
		$scope.ctrlFechah = {
			opened : false,
			open:function(){
				this.opened = true;
			}
		};
		$scope.dateOptionsD = {
			formatYear: 'yy',
			startingDay: 1
		};
		$scope.dateOptionsH = {
			formatYear: 'yy',
			startingDay: 1
		};
		$scope.nuevo = function(){
			$state.go('home.nuevo');
		};

		$scope.search = function(){
			$state.go('home.busqueda', {
				fechad: $filter("date")($scope.fechad, "yyyy-MM-dd"),
				fechah: $filter("date")($scope.fechah, "yyyy-MM-dd")
			});
		};

		$scope.export = function(){
			LegajoService.exportar($filter("date")($scope.fechad, "yyyy-MM-dd"),$filter("date")($scope.fechah, "yyyy-MM-dd"));
		};

		$scope.ver = function(pos){
			var legajo = $scope.bsTable.dtInstance.DataTable.data()[pos];
			$window.open(legajo.url, '_blank');
		};
		$scope.eliminar = function(pos){
			var legajo = $scope.bsTable.dtInstance.DataTable.data()[pos];
			$rootScope.confirmMsg = 'Esta seguro que desea eliminar el Legajo ' + legajo.numerolegajo;
			$rootScope.confirmTitle = 'Legajo';
			$uibModal.open({
				size:'modal-sm',
				templateUrl : AppConfig.url + '/templates/modal/confirm.tpl.html'
			}).result.then(function(){
				LegajoService.borrar(legajo).then(function(){
					$scope.bsTable.dtInstance.reloadData();
				});
			},function(){

			});
		};
	});

	docsViewApp.controller('NuevoController',function($state,$scope,LegajoService){
		$scope.upload = {};
		$scope.metas = [];
		angular.forEach(AppConfig.params.metas,function(key,index){
			$scope.metas.push({key:key,label:AppConfig.params.metas_title[index]});
		});
		$scope.cargar = function(){
			$scope.loading = true;
			LegajoService.nuevo($scope.upload).then(function(){
				$scope.loading = false;
				$state.go('home.listado');
			},function(){
				$scope.loading = false;
				$scope.errormsj = "No se pudo cargar el Legajo";
			});
		};
	});

	docsViewApp.service('UserService',function($q,$http,$httpParamSerializer){
		var userService = {};
		userService.login = function(user){
			var deferred =  $q.defer();
			try{
				var data = angular.extend(user,{
					action:'docsview_login'
				});
				$http.post(AppConfig.ajaxurl,$httpParamSerializer(data),{
					headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function(response){
					deferred.resolve(response.data);
				},function(response){
					deferred.reject(response.data);
				});
			}catch(err){
				deferred.reject({message:err});
			}
			return deferred.promise;
		};
		userService.logout = function(){
			return $http.get(AppConfig.ajaxurl + '?action=docsview_logout');
		};
		return userService;
	});

	docsViewApp.service('UploadService',function($http,$q){
		var uploadService = {};
		uploadService.nuevo = function(upload){
			var deferred =  $q.defer();
			var fd = new FormData();
	        fd.append('file', upload.file);
	        fd.append('action', 'docsview_upload');
	       	angular.forEach(AppConfig.params.metas,function(key,index){
	       		fd.append('meta_'+key, upload[key]);
			});
	        $http.post(AppConfig.ajaxurl, fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        }).then(function(response){
	        	console.log(response);
	        	deferred.resolve();
	        },function(response){
				console.log(response);
				deferred.reject();
	        });	
	        return deferred.promise;
		};
		
        return uploadService;
	});

	docsViewApp.service('LegajoService',function($http,$q,$httpParamSerializer,$window){
		var legajoService = {};
		
		legajoService.nuevo = function(upload){
			var deferred =  $q.defer();
			var fd = new FormData();
	        fd.append('file', upload.file);
	        fd.append('action', 'docsview_upload');
        	angular.forEach(AppConfig.params.metas,function(key,index){
       			fd.append('meta_'+key, upload[key]);
			});
	        $http.post(AppConfig.ajaxurl, fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        }).then(function(response){
	        	console.log(response);
	        	deferred.resolve();
	        },function(response){
				console.log(response);
				deferred.reject();
	        });	
	        return deferred.promise;
		};

		legajoService.borrar = function(legajo){	
			var data = {
				action: 'docsview_delete',
				id: legajo.id
			};
			return $http.post(AppConfig.ajaxurl,$httpParamSerializer(data),{
				headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			});
		};

		legajoService.listado = function(fechad,fechah){	
			var deferred =  $q.defer();
			var data = {
				action: 'docsview_list'
			};
			if(fechad){
				data.fechad = fechad;
			}
			if(fechah){
				data.fechah = fechah;
			}
			var query = $httpParamSerializer(data);
			$http.get(AppConfig.ajaxurl + '?' + query ).then(function(response){
				deferred.resolve(response.data);
			},function(response){
				deferred.reject();
			});
			return deferred.promise;
		};

		legajoService.exportar = function(fechad,fechah){	
			var data = {
				action: 'docsview_export'
			};
			if(fechad){
				data.fechad = fechad;
			}
			if(fechah){
				data.fechah = fechah;
			}
			var query = $httpParamSerializer(data);
			$window.location.href = AppConfig.ajaxurl + '?' + query;
		};
		
        return legajoService;
	});

	docsViewApp.directive('fileModel', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function(scope, element, attrs) {
	            var model = $parse(attrs.fileModel);
	            var modelSetter = model.assign;
	            
	            element.bind('change', function(){
	                scope.$apply(function(){
	                    modelSetter(scope, element[0].files[0]);
	                });
	            });
	        }
	    };
	}]);

})(angular.module( 'docsViewApp' , [ 'ui.router' , 'ui.bootstrap' , 'datatables', 'datatables.bootstrap'] ));


